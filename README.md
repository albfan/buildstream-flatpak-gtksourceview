#Building gtksourceview with Buildstream

This example project build gtksourceview through BuildStream using GNOME flatpak runtime for deps

## Building

```
bst build --track-all gtksourceview.bst
bst shell --build gtksourceview.bst
./autogen.sh
./configure
make
```

To try tes suite (from bst shell):

```
cd tests
./test-space-drawing
```

